package com.example.assignment_1;

public class user {
	
	//private variables
	int id;
	String name;
	//empty constructor
	public user(){
		
	}
	//constructor
	public user(int id, String name){
		this.id=id;
		this.name=name;
		
	}
	//getter's and setter's
	public int getID(){
		return this.id;
	}
	public void setID(int id){
		this.id=id;
	}
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name=name;
	}
	

}
