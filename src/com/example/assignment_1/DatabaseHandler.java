package com.example.assignment_1;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
	
	private static final int DATABASE_VERSION=1;
	private static final String DATABASE_NAME="UserName";
	private static final String TABLE_USERS="users";
	
	private static final String KEY_ID="id";
	private static final String KEY_NAME="name";
	//class that handles databaseoperations
	public DatabaseHandler(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	@Override
	public void onCreate(SQLiteDatabase db){
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT"
                +  ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
	}
	@Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
 
        // Create tables again
        onCreate(db);
    }
	// Adding new user
	public void addUser(user _user) {
		SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_NAME, _user.getName()); // Contact Name
	    
	    // Inserting Row
	    db.insert(TABLE_USERS, null, values);
	    db.close(); // Closing database connection
	}
	 
	// Getting single user
	public user getUser(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		 
	    Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_ID,
	            KEY_NAME}, KEY_ID + "=?",
	            new String[] { String.valueOf(id) }, null, null, null, null);
	    if (cursor != null)
	        cursor.moveToFirst();
	 
	    user _user = new user(Integer.parseInt(cursor.getString(0)),
	            cursor.getString(1));
	    // return contact
	    return _user;
	}
	 
	// Getting All Contacts
	public List<user> getAllContacts() {
		List<user> _userList = new ArrayList<user>();
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + TABLE_USERS;
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) {
	        do {
	            user _user = new user();
	            _user.setID(Integer.parseInt(cursor.getString(0)));
	            _user.setName(cursor.getString(1));
	            // Adding contact to list
	            _userList.add(_user);
	        } while (cursor.moveToNext());
	    }
	 
	    // return contact list
	    return _userList;
	}
	 
	// Getting user Count
	public int getUsersCount() {
		String countQuery = "SELECT  * FROM " + TABLE_USERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int temp_count=cursor.getCount();
        cursor.close();
 
        // return count
        return temp_count;
	}
	// Updating single contact
	public int updateUser(user _user) {
		SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_NAME, _user.getName());
	 
	    // updating row
	    return db.update(TABLE_USERS, values, KEY_ID + " = ?",
	            new String[] { String.valueOf(_user.getID()) });
	}
	 
	// Deleting single contact
	public void deleteUser(user _user) {
		SQLiteDatabase db = this.getWritableDatabase();
	    db.delete(TABLE_USERS, KEY_ID + " = ?",
	            new String[] { String.valueOf(_user.getID()) });
	    db.close();
	}
}
