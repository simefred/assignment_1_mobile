package com.example.assignment_1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.example.assignment_1.R;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.widget.ImageView;
//This code is written by Simon McCallum small alterations has been done by Simen Christoffer Fredriksen
public class Picture_download extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_picture_download);
		
		//This is where the image is loaded
	    //Sending any URL pointing to an image will work as the bitmap class reads common formats		
	    Bitmap bitmap = DownloadImage("http://www.ansatt.hig.no/simonm/images/VikingMe150.png");
	    //Then display the image to a view
	    
	    ImageView img = (ImageView) findViewById(R.id.img);
	    img.setImageBitmap(bitmap);
	}

	

/**
 * @author Simon McCallum
 *
 *This function downloads the image at the URL
 *location passed and then returns the bitmap
 * @param  URL     an absolute URL giving the base location and name of the image
 * @return bitmap  the image at the specified URL
 *
 */
private Bitmap DownloadImage(String URL)
{        
    Bitmap bitmap = null;
    InputStream in = null;        
    try {
        in = OpenHttpConnection(URL);
        bitmap = BitmapFactory.decodeStream(in);
        in.close();
    } catch (IOException e1) {
        e1.printStackTrace();
    }
    return bitmap;                
}
/**
 * @author Simon McCallum
 *
 * Handles some of the complexity of opening up a HTTP connection
 * @param  URL   a String containing the absolute URL giving the base location and name of the content
 * @return in    an inputStream which will be the stream of text from the server
 *
 */    
private InputStream OpenHttpConnection(String urlString) 
throws IOException
{
    InputStream in = null;
    int response = -1;
           
    URL url = new URL(urlString); 
    URLConnection conn = url.openConnection();
             
    if (!(conn instanceof HttpURLConnection))                     
        throw new IOException("Not an HTTP connection");
    
    try{
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false);
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setRequestMethod("GET");
        httpConn.connect(); 

        response = httpConn.getResponseCode();                 
        if (response == HttpURLConnection.HTTP_OK) {
            in = httpConn.getInputStream();                                 
        }                     
    }
    catch (Exception ex)
    {
        throw new IOException("Error connecting");            
    }
    return in;     
}

}