package com.example.assignment_1;
import android.util.Log;
import android.view.View;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        DatabaseHandler db = new DatabaseHandler(this);
        int temp_int=db.getUsersCount();
        if(temp_int>0){
        	user last_user=db.getUser(temp_int);
        	((TextView)findViewById(R.id.current_user)).setText("Welcome "+last_user.getName());
        }
        else{
        	((TextView)findViewById(R.id.current_user)).setText("Please set username");
        }
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    	//Starts the Picture download
    public void download_picture(View view){
    	Intent intent = new Intent(this, Picture_download.class);
    	startActivity(intent);

    }
    //intent for accessing the Username activity
    public void user_name(View view){
    	Intent intent =new Intent(this,Username.class);
    	startActivity(intent);
    	
    }
    //intent for accessing the State_save activity
    public void state_save(View view){
    	Intent intent = new Intent(this,State_save.class);
    	startActivity(intent);
    }
}
