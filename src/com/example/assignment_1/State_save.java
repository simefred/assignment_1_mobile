package com.example.assignment_1;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;

public class State_save extends Activity {
	//name of the prefernce file
	public static final String PREFS_NAME = "MyPrefsFile";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_state_save);
		
	}
	//overriding onResume to get the saved setting
	//from on Pause.
	@Override
	protected void onResume(){
		super.onResume();
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    String saved_text = " ";
	    saved_text=settings.getString("saved_text",saved_text);
	    ((TextView)findViewById(R.id.state_text)).setText(saved_text);
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.state_save, menu);
		
		return true;
	}
	//overriding onPause to save a String
	@Override
	protected void onPause(){
        super.onPause();
        EditText tempText= (EditText) findViewById(R.id.input_text_state);
        ((TextView)findViewById(R.id.state_text)).setText(tempText.getText().toString());
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("saved_text", tempText.getText().toString());
        editor.commit();

	}
}
